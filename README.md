# How to run

''''
(ephemeris-py3.10) dom@x1-vm:~/ephemeris/ephemeris$ uvicorn fastapi_app:app --reload

(ephemeris-py3.10) dom@x1-vm:~/ephemeris/ephemeris$ python fastapi_app.py 

''''

# Swagger UI

http://127.0.0.1:8000/docs


# Divers

- https://towardsdatascience.com/implementing-fastapi-in-10-minutes-d161cdd7c075
- https://christophergs.com/tutorials/ultimate-fastapi-tutorial-pt-4-pydantic-schemas/
- 

# Rise and set for sun or moon

https://aa.usno.navy.mil/faq/RST_defs

Rise, Set: During the course of a day the Earth rotates once on its axis causing the phenomena of rising and setting. Excluding circumpolar objects, celestial bodies – stars and planets included – seem to appear in the sky at the horizon to the East of any particular place, then to cross the sky and again disappear at the horizon to the West. The most noticeable of these events, and the most significant in regard to ordinary affairs, are the rising and setting of the Sun and Moon. Because the Sun and Moon appear as circular disks and not as points of light, a definition of rise or set must be very specific because not all of either body is seen to rise or set at once.

    Sunrise and sunset conventionally refer to the times when the upper edge of the disk of the Sun is on the horizon. Atmospheric conditions are assumed to be average, and the location is in a level region on the Earth's surface.

    Moonrise and moonset times are computed for exactly the same circumstances as for sunrise and sunset. However, moonrise and moonset may occur at any time of day and, consequently, it is often possible for the Moon to be seen during daylight, and to have moonless nights. It is also possible that a moonrise or moonset does not occur relative to a specific place on a given date.

