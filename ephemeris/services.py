from skyfield import api, VERSION, almanac, units
from datetime import datetime, timedelta
from pytz import timezone
from typing import Union

# import json

from station import sta_lat_lon


class Almanac:
    def __init__(self):
        self.ts = api.load.timescale()
        self.eph = api.load("de421.bsp")

    def moon_phase_angle(self, timestamp: datetime) -> units.Angle:
        t = self.ts.utc(timestamp)
        angle = almanac.moon_phase(self.eph, t)
        return angle

    def sunrise_sunset(self, timestamp: datetime, st_abbr: str) -> tuple[datetime, int]:
        start_time = self.ts.utc(timestamp)
        end_time = start_time + timedelta(days=1)
        (lat, lon) = sta_lat_lon(st_abbr)
        # use api.N and api.E to specify North and East
        station = api.wgs84.latlon(lat * api.N, lon * api.E)
        # calculate the sunrise and sunset
        sun_time, up = almanac.find_discrete(
            start_time, end_time, almanac.sunrise_sunset(self.eph, station)
        )
        # convert sunrise and sunset as datetime
        sun_time_as_datetime = []
        for st in sun_time:
            sun_time_as_datetime.append(st.utc_datetime())
        # convert numpy.int_8 to int
        boolean_up = []
        for bool in up:
            boolean_up.append(int(bool))
        return sun_time_as_datetime, boolean_up

    def moonrise_moonset(
        self, timestamp: datetime, st_abbr: str
    ) -> tuple[Union[datetime, str], Union[int, None]]:
        t0 = self.ts.utc(timestamp)
        t1 = t0 + timedelta(days=1)
        (lat, lon) = sta_lat_lon(st_abbr)
        # use api.N and api.E to specify North and East
        station = api.wgs84.latlon(lat * api.N, lon * api.E)
        moon_almanac = almanac.risings_and_settings(
            self.eph, self.eph["Moon"], station, radius_degrees=0.25
        )
        t, y = almanac.find_discrete(t0, t1, moon_almanac)
        tt = [None, None]
        yy = [None, None]
        if len(y) == 1:  # rise and set not the same day
            if y[0] == 1:  # only a rise in the day
                tt[0] = t[0].utc_datetime()
                tt[1] = "NoSet"
                yy[0] = 1
            else:  # only a set in the day
                print(f"t:{t[0].tt}")
                tt[0] = "NoRise"
                tt[1] = t[0].utc_datetime()
                yy[1] = 0
        else:
            tt[0] = t[0].utc_datetime()
            tt[1] = t[1].utc_datetime()
            yy[0] = int(y[0])
            yy[1] = int(y[1])
        return tt, yy


def skyfiled_version():
    return VERSION


def moon_phase_percent(moon_phase: almanac.moon_phase) -> int:
    """
    Return moon phase in percent
    """
    angle = float(moon_phase.degrees)
    if angle <= 180.0:
        percent_illuminated = angle * 100.0 / 180.0
    else:
        percent_illuminated = (180 - (angle % 180.0)) * 100.0 / 180.0
    return int(round(percent_illuminated))


def lunar_day(moon_phase: almanac.moon_phase) -> float:
    """
    Return the lunar day according. 1 lunar day is 29.530588 days correspond to 360.0 degrees on the moon phase
    """
    angle = float(moon_phase.degrees)
    ld = (angle * 29.530588) / 360.0
    return ld


def moon_phase(angle: almanac.moon_phase) -> int:
    """
    Returns the moon phase according to the 8 moon phases
    see https://www.omnicalculator.com/everyday-life/moon-phase
    """
    ld = lunar_day(angle)
    print(f"lunar_day: {ld}")
    if ld <= 1.0:
        mp = 0
    elif ld <= 6.382647:
        mp = 1
    elif ld <= 8.382647:
        mp = 2
    elif ld <= 13.765294:
        mp = 3
    elif ld <= 15.765294:
        mp = 4
    elif ld <= 21.147941:
        mp = 5
    elif ld <= 23.147941:
        mp = 6
    elif ld <= 28.530588:
        mp = 7
    elif ld <= 29.530588:
        mp = 0
    else:
        mp = -1  # error with moon phase
    return mp


def moon_phase_name(moon_phase: int) -> str:
    moon_phase_names = [
        "New Moon",
        "Waxing Crescent",
        "First Quarter",
        "Waxing Gibbous",
        "Full Moon",
        "Waning Gibbous",
        "Last Quarter",
        "Waning Crescent",
    ]
    return moon_phase_names[moon_phase]
