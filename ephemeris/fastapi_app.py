from fastapi import FastAPI, APIRouter
from pydantic import BaseModel
import typing

from station import stations
from schemas import Station, Sun_almanac, Moon_almanac, Moon_phase
import services
import time_calculation

# initialise Almanac
almanac = services.Almanac()

# initialise timezone to CET/CEST
cet = time_calculation.local_time("Europe/Zurich")

# Define an API object
app = FastAPI(title="Ephemeris for MCH stations")

api_router = APIRouter()


# Define data type
class Msg(BaseModel):
    msg: str


# Map HTTP method and path to python function
@api_router.get("/", status_code=200)
async def root() -> dict:
    """
    Http root, return just an info message
    """
    return {"message": "Ephemeris for Moon and Sun"}


@api_router.get("/station/{station_abbr}", status_code=200, response_model=Station)
async def station_info(station_abbr: str):
    """
    Display info station

    Parameters:
        station_abbr (str): MCH station abbreviation

    Returns:
        station info (dict): station info from station.py
    """
    sta = [station for station in stations if station["abbr"] == station_abbr]

    return sta[0]


@api_router.get("/sunrise_sunset", status_code=200, response_model=Sun_almanac)
async def sunriseset(year: int, month: int, day: int, abbr: str) -> dict[str, str]:
    """
    Returns sunrise/sunset and day length

    Parameters:
        year (int): 4 digits year
        month (int): month (1..12)
        day (int): day (1..31)
        abbr (str): station abbreviation

    Returns:
        sunrise (str): sunrise time in lt as HH:MM
        sunset (str): sunset time in lt as HH:MM
        daylength (str): length of the day in HH:MM:SS
    """
    dt = cet.aware_datetime(year, month, day)
    yesterday = time_calculation.yesterday(dt)
    sun, up = almanac.sunrise_sunset(dt, st_abbr=abbr)
    sun_yesterday, up_yesterday = almanac.sunrise_sunset(yesterday, st_abbr=abbr)
    sun_lt = cet.to_lt(sun)
    hours, minutes, seconds = time_calculation.time_diff(sun[0], sun[1])
    day_diff = time_calculation.day_diff(
        sun[0], sun[1], sun_yesterday[0], sun_yesterday[1]
    )
    ephemeride = {
        "sunrise": time_calculation.nearest_minute(sun_lt[0]).strftime("%H:%M"),
        "sunset": time_calculation.nearest_minute(sun_lt[1]).strftime("%H:%M"),
        "daylength": f"{hours}:{minutes}:{seconds}",
        "daydiff": day_diff,
    }
    return ephemeride
    # return sun_lt, up, hours, minutes, seconds


@api_router.get("/moonrise_moonset", status_code=200, response_model=Moon_almanac)
async def moonriseset(year: int, month: int, day: int, abbr: str) -> dict[str, str]:
    """
    Returns moonrise/moonset
    """
    dt = cet.aware_datetime(year, month, day)
    moon, up = almanac.moonrise_moonset(dt, st_abbr=abbr)
    moon_lt = cet.to_lt(moon)
    ephemeride = {}
    if moon_lt[0] == "NoRise":
        ephemeride["moonrise"] = moon_lt[0]
    elif moon_lt[0] == "NoSet":
        ephemeride["moonset"] = moon_lt[0]
    else:
        ephemeride["moonrise"] = time_calculation.nearest_minute(moon_lt[0]).strftime(
            "%H:%M"
        )
    if moon_lt[1] == "NoRise":
        ephemeride["moonrise"] = moon_lt[1]
    elif moon_lt[1] == "NoSet":
        ephemeride["moonset"] = moon_lt[1]
    else:
        ephemeride["moonset"] = time_calculation.nearest_minute(moon_lt[1]).strftime(
            "%H:%M"
        )
    return ephemeride
    # return moon_lt, up


@api_router.get("/moon_phase", status_code=200, response_model=Moon_phase)
async def moon_phase(year: int, month: int, day: int) -> dict[int, str, int]:
    """
    Returns moon phase (0 to 360) with 0 new moon and 180 full moon
    """
    dt = cet.aware_datetime(year, month, day)
    angle = almanac.moon_phase_angle(dt)
    percent = services.moon_phase_percent(angle)
    moonphase = services.moon_phase(angle)
    moonphasename = services.moon_phase_name(moonphase)
    return {"moonphase": moonphase, "moonphasename": moonphasename, "percent": percent}


@api_router.get("/skyfield_version", status_code=200)
async def skyfield_version() -> list[int, int]:
    """
    Return skyfiled version number
    """
    version = services.skyfiled_version()
    return version


app.include_router(api_router)

if __name__ == "__main__":
    import uvicorn

    uvicorn.run(app, host="0.0.0.0", port=8001, log_level="debug")
