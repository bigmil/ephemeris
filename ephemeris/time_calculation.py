import datetime
from pytz import timezone
from typing import Tuple


def time_diff(
    begin_time: datetime.datetime, end_time: datetime.datetime
) -> Tuple[int, int, int]:
    """
    Return the time period in hours, minutes and seconds

    Parameters:
        begin_time (datetime.datetime): begin time
        end_time (datetime.datetime): end time

    Returns:
        hours (int): number of hours between begin and end time
        minutes (int): number of minutes between begin and end time (minus hours*60)
        seconds (int): number of remaning seconds (rounded and converted to int
    """
    period = end_time - begin_time
    print(f"period: {period}")
    total_seconds = period.total_seconds()
    minutes, seconds = divmod(total_seconds, 60)
    hours, minutes = divmod(minutes, 60)

    return int(hours), int(minutes), int(round(seconds, 0))


def day_diff(
    today_begin: datetime.datetime,
    today_end: datetime.datetime,
    yesterday_begin: datetime.datetime,
    yesterday_end: datetime.datetime,
) -> str:
    today = today_end - today_begin
    today_sec = today.total_seconds()
    yesterday = yesterday_end - yesterday_begin
    yesterday_sec = yesterday.total_seconds()
    diff_sec = today_sec - yesterday_sec
    sign = "-" if diff_sec < 0 else "+"
    minutes, seconds = divmod(abs(diff_sec), 60)
    return f"{sign}{minutes:02.0f}:{seconds:02.0f}"


class local_time:
    def __init__(self, time_zone):
        self.time_zone = timezone(time_zone)

    def to_lt(self, utc_date_time: tuple):
        local_time = []
        for utc_dt in utc_date_time:
            if (
                type(utc_dt) is datetime.datetime
            ):  # do not convert if not a datetime, means that it is NoSet or NoRise
                local_time.append(utc_dt.astimezone(self.time_zone))
            else:
                local_time.append(utc_dt)
        return local_time

    def to_utc(self, lt_date_time: tuple):
        utc_time = []
        for lt_dt in lt_date_time:
            utc_time.append(lt_dt.astimezone(timezone("UTC")))
        return utc_time

    def aware_datetime(self, year, month, day, hour=0):
        """
        Return an aware datetime object
        """
        dt = datetime.datetime(year, month, day, hour)
        dt = dt.astimezone(self.time_zone)
        return dt


def nearest_minute(dt):
    """
    Returns nearest minute.
    dt: datetime
    The results should then agree with the tables produced by the USNO."""
    return (dt + datetime.timedelta(seconds=30)).replace(second=0, microsecond=0)


def yesterday(dt: datetime.datetime) -> datetime.datetime:
    """
    Returns yesterday datetime
    """
    return dt + datetime.timedelta(days=-1)
