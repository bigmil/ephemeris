from pydantic import BaseModel


class Station(BaseModel):
    abbr: str
    name: str
    lat: str
    lon: str
    lv03: str
    alt: int


class Sun_almanac(BaseModel):
    sunrise: str
    sunset: str
    daylength: str
    daydiff: str


class Moon_almanac(BaseModel):
    moonrise: str
    moonset: str


class Moon_phase(BaseModel):
    moonphase: int
    moonphasename: str
    percent: int
