stations = [
    {
        "abbr": "GVE",
        "name": "Genève / Cointrin",
        "lat": "46.247506912",
        "lon": "6.127757732",
        "lv03": "411 498905 122632",
        "alt": 460,
    },
    {
        "abbr": "KLO",
        "name": "Zürich / Kloten",
        "lat": "47.479615988",
        "lon": "8.536069897",
        "lv03": "426 682718 259339",
        "alt": 473,
    },
    {
        "abbr": "OTL",
        "name": "Locarno / Monti",
        "lat": "46.172262800",
        "lon": "8.787498541",
        "lv03": "367 704167 114317",
        "alt": 417,
    },
    {
        "abbr": "BER",
        "name": "Berne / Zollikofen",
        "lat": "46.990749748",
        "lon": "7.464056891",
        "lv03": "553 601934 204410",
        "alt": 601,
    },
    {
        "abbr": "SIO",
        "name": "Sion",
        "lat": "46.218652486",
        "lon": "7.330207227",
        "lv03": "482 591633 118584",
        "alt": 534,
    },
]


def sta_lat_lon(station_name):
    for station in stations:
        if station["abbr"] == station_name:
            lat = station["lat"]
            lon = station["lon"]
            break
    return float(lat), float(lon)
